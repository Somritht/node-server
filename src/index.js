var express = require("express");
var app = express();
var bodyParser = require('body-parser')
var jwt = require('jsonwebtoken');
var faker = require('faker');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
const jwt_secret = 'QXij3829QJNISCIN890S';

app.get("/url", (req, res, next) => {
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
});


app.post("/login", (req,res,next)=> {
    const { email , password } = req.body;
    if(!(email === 'admin@gmail.com' && password === '12345'))
        res.status(400).send({ 'message' : 'not have permission to access'  })
   
    let token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60 * 60),
        data: 'foobar'
      }, jwt_secret);
    res.status(200).send({...req['body'], token });
});

app.get("/verify",(req,res,next) => {
    try {
        var decoded = jwt.verify(req.headers['token'], jwt_secret);
    res.status(200).send(decoded);
    } catch (error) {
    res.status(400).send({ 'message' : 'not have permission to access' , 'err' : error  });
    }  
});

app.get("/admins",(req,res,next) => {
    let users = [];
    for (let index = 0; index < 100; index++) {
        users.push({
            'name' : faker.name.findName(),
            'photo' : faker.image.people(),
            'email' : faker.internet.email(),
            'phone' : faker.phone.phoneNumberFormat(),
            'job' : faker.name.jobTitle(),
            'created_date' : faker.date.recent()
        });    
    }
    res.status(200).send(users);
});

app.get("/products",(req,res,next) => {
    let users = [];
    for (let index = 0; index < 100; index++) {
        users.push({
            'name' : faker.commerce.productName(),
            'photo' : faker.image.people(),
            'price' : faker.commerce.price(),
            'color' : faker.commerce.color(),
            'description' : faker.commerce.productDescription(),
            'created_date' : faker.date.recent()
        });    
    }
    res.status(200).send(users);
});
app.get("/vehicles",(req,res,next) => {
    let users = [];
    for (let index = 0; index < 100; index++) {
        users.push({
            'name' : faker.vehicle.vehicle(),
            'manufacturer' : faker.vehicle.manufacturer(),
            'model' : faker.vehicle.model(),
            'type' : faker.vehicle.type(),
            'vin' : faker.vehicle.vin(),
            'created_date' : faker.date.recent()
        });    
    }
    res.status(200).send(users);
});

app.listen(3000, () => {
 console.log("Server running on port 3000");
});