export default {
    env: 'development',
    db: '',
    port: 3000,
    jwtSecret: 'my-api-secret',
    jwtDuration: '2 hours'
};